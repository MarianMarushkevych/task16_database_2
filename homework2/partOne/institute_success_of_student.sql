create database institute_success_of_student;
use institute_success_of_student;
create table studet
(
  idStudent int   AUTO_INCREMENT PRIMARY KEY,
  firstname varchar(25) not null,
  lasrname varchar(25) not null,
  surname varchar(25) not null,
  autobiograhy nvarchar(8000) null,
  adress varchar(100) not null,
  studentship decimal(3,2) default 720.16,
  foto longblob null,
  year_entry year not null,
  birthday year not null,
  rating int not null,
  idGroup_identeficator int
  
)Engine InnoDB auto_increment 1;


create table group_identeficator
(
  idGroup_identeficator int primary key,
  identeficatorGRP varchar(10) 
)engine InnoDB;


create table study_predmet
(
 idPredmet int primary key,
 namePredmet varchar(25) ,
 idGroup_identeficator int
)engine InnoDB;


create table modul_first
(
 idModul_first int primary key,
 namePred varchar(25),
 typeControl varchar(10),
 result_100 int,
 term int,
 result_50 int,
 idStudent int
 )engine InnoDB;
 
 
create table module_second
(
 idModul_second int primary key,
 namePred varchar(25),
 typeControl varchar(10),
 result_100 int,
 term int,
 result_50 int,
 idStudent int
)engine InnoDB;


create table prepod
(
   idPrepod int primary key,
   namePrepod varchar(25),
   idPredmet int

)engine InnoDB;


ALTER TABLE studet
ADD CONSTRAINT FK_student_groupIdenteficator
FOREIGN KEY ( idGroup_identeficator)
REFERENCES group_identeficator (idGroup_identeficator)
ON DELETE CASCADE ON UPDATE SET NULL ;


ALTER TABLE study_predmet
ADD CONSTRAINT FK_study_predmet_groupIdenteficator
FOREIGN KEY ( idGroup_identeficator)
REFERENCES group_identeficator (idGroup_identeficator)
ON DELETE CASCADE ON UPDATE SET NULL ;


ALTER TABLE modul_first
ADD CONSTRAINT FK_module_first_Student
FOREIGN KEY ( idStudent)
REFERENCES studet (idStudent)
ON DELETE CASCADE ON UPDATE SET NULL ;


ALTER TABLE module_second
ADD CONSTRAINT FK_module_second_Student
FOREIGN KEY ( idStudent)
REFERENCES studet (idStudent)
ON DELETE CASCADE ON UPDATE SET NULL ;


ALTER TABLE prepod
ADD CONSTRAINT FK_prepod_Predmet
FOREIGN KEY ( idPredmet)
REFERENCES study_predmet (idPredmet)
ON DELETE CASCADE ON UPDATE SET NULL ;


