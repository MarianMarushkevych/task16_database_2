-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema AppleShopProject
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema AppleShopProject
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `AppleShopProject` DEFAULT CHARACTER SET utf8 ;
USE `AppleShopProject` ;

-- -----------------------------------------------------
-- Table `AppleShopProject`.`street`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AppleShopProject`.`street` (
  `street` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`street`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AppleShopProject`.`apple_shop`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AppleShopProject`.`apple_shop` (
  `id` INT NOT NULL,
  `name` VARCHAR(15) NOT NULL,
  `building_number` VARCHAR(10) NULL,
  `www` VARCHAR(40) NULL,
  `work_time` TIME NULL,
  `saturday` BIT(1) NULL,
  `sunday` BIT(1) NULL,
  `street` VARCHAR(25) NULL,
  `street_street` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`id`, `street_street`),
  INDEX `fk_apple_shop_street1_idx` (`street_street` ASC) VISIBLE,
  CONSTRAINT `fk_apple_shop_street1`
    FOREIGN KEY (`street_street`)
    REFERENCES `AppleShopProject`.`street` (`street`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AppleShopProject`.`post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AppleShopProject`.`post` (
  `post` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`post`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AppleShopProject`.`employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AppleShopProject`.`employee` (
  `id` INT NOT NULL,
  `surname` VARCHAR(30) NOT NULL,
  `name` CHAR(30) NOT NULL,
  `midle_name` VARCHAR(30) NULL,
  `identity_number` CHAR(10) NULL,
  `passport` CHAR(10) NULL,
  `experience` DECIMAL(10,1) NULL,
  `birthday` DATE NULL,
  `post` VARCHAR(15) NOT NULL,
  `apple_id` INT NULL,
  `apple_shop_id` INT NOT NULL,
  `post_post` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`, `apple_shop_id`, `post_post`),
  INDEX `fk_employee_apple_shop_idx` (`apple_shop_id` ASC) VISIBLE,
  INDEX `fk_employee_post1_idx` (`post_post` ASC) VISIBLE,
  CONSTRAINT `fk_employee_apple_shop`
    FOREIGN KEY (`apple_shop_id`)
    REFERENCES `AppleShopProject`.`apple_shop` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_employee_post1`
    FOREIGN KEY (`post_post`)
    REFERENCES `AppleShopProject`.`post` (`post`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AppleShopProject`.`apple_shop_repair`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AppleShopProject`.`apple_shop_repair` (
  `apple_shop_id` INT NOT NULL,
  `repair_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`apple_shop_id`, `repair_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AppleShopProject`.`zone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AppleShopProject`.`zone` (
  `id` INT NOT NULL,
  `name` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AppleShopProject`.`repair_zone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AppleShopProject`.`repair_zone` (
  `repair_id` INT NOT NULL,
  `zone_id` VARCHAR(45) NOT NULL,
  `zone_id1` INT NOT NULL,
  PRIMARY KEY (`repair_id`, `zone_id`, `zone_id1`),
  INDEX `fk_repair_zone_zone1_idx` (`zone_id1` ASC) VISIBLE,
  CONSTRAINT `fk_repair_zone_zone1`
    FOREIGN KEY (`zone_id1`)
    REFERENCES `AppleShopProject`.`zone` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AppleShopProject`.`repair`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AppleShopProject`.`repair` (
  `id` INT NOT NULL,
  `name` VARCHAR(30) NOT NULL,
  `guaranty_code` CHAR(10) NULL,
  `guarantee` BIT(1) NULL,
  `replace_of_part` BIT(1) NULL,
  `repair_of_part` BIT(1) NULL,
  `repair_zone_repair_id` INT NOT NULL,
  `repair_zone_zone_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `repair_zone_repair_id`, `repair_zone_zone_id`),
  INDEX `fk_repair_repair_zone1_idx` (`repair_zone_repair_id` ASC, `repair_zone_zone_id` ASC) VISIBLE,
  CONSTRAINT `fk_repair_repair_zone1`
    FOREIGN KEY (`repair_zone_repair_id` , `repair_zone_zone_id`)
    REFERENCES `AppleShopProject`.`repair_zone` (`repair_id` , `zone_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AppleShopProject`.`apple_shop_has_apple_shop_repair`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AppleShopProject`.`apple_shop_has_apple_shop_repair` (
  `apple_shop_id` INT NOT NULL,
  `apple_shop_repair_apple_shop_id` INT NOT NULL,
  `apple_shop_repair_repair_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`apple_shop_id`, `apple_shop_repair_apple_shop_id`, `apple_shop_repair_repair_id`),
  INDEX `fk_apple_shop_has_apple_shop_repair_apple_shop_repair1_idx` (`apple_shop_repair_apple_shop_id` ASC, `apple_shop_repair_repair_id` ASC) VISIBLE,
  INDEX `fk_apple_shop_has_apple_shop_repair_apple_shop1_idx` (`apple_shop_id` ASC) VISIBLE,
  CONSTRAINT `fk_apple_shop_has_apple_shop_repair_apple_shop1`
    FOREIGN KEY (`apple_shop_id`)
    REFERENCES `AppleShopProject`.`apple_shop` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_apple_shop_has_apple_shop_repair_apple_shop_repair1`
    FOREIGN KEY (`apple_shop_repair_apple_shop_id` , `apple_shop_repair_repair_id`)
    REFERENCES `AppleShopProject`.`apple_shop_repair` (`apple_shop_id` , `repair_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AppleShopProject`.`apple_shop_repair_has_repair`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AppleShopProject`.`apple_shop_repair_has_repair` (
  `apple_shop_repair_apple_shop_id` INT NOT NULL,
  `apple_shop_repair_repair_id` VARCHAR(45) NOT NULL,
  `repair_id` INT NOT NULL,
  PRIMARY KEY (`apple_shop_repair_apple_shop_id`, `apple_shop_repair_repair_id`, `repair_id`),
  INDEX `fk_apple_shop_repair_has_repair_repair1_idx` (`repair_id` ASC) VISIBLE,
  INDEX `fk_apple_shop_repair_has_repair_apple_shop_repair1_idx` (`apple_shop_repair_apple_shop_id` ASC, `apple_shop_repair_repair_id` ASC) VISIBLE,
  CONSTRAINT `fk_apple_shop_repair_has_repair_apple_shop_repair1`
    FOREIGN KEY (`apple_shop_repair_apple_shop_id` , `apple_shop_repair_repair_id`)
    REFERENCES `AppleShopProject`.`apple_shop_repair` (`apple_shop_id` , `repair_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_apple_shop_repair_has_repair_repair1`
    FOREIGN KEY (`repair_id`)
    REFERENCES `AppleShopProject`.`repair` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
